package com.sda.swing.Filmy;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class MovieTableModel extends AbstractTableModel {
    private final String[] columnNames =
            new String[] { "Tytuł", "Reżyser", "Gatunek", "Ocena", "Rok" };
    private List<Movie> listaFilmow;

    @Override
    public int getRowCount() {
        return listaFilmow.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        return null;
    }
}
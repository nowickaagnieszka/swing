package com.sda.swing.Filmy;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class WindowPanel {
    private JPanel panel1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextArea tytułTextArea2;
    private JTextArea reżyserTextArea;
    private JTextArea rokTextArea;
    private JTextField textField4;
    private JTextArea gatunekTextArea;
    private JTextArea ocenaTextArea;
    private JTextField textField5;
    private JTextField textField6;
    private JButton dodajButton;
    private JButton usuńButton;
    private JButton zapiszButton;
    private JButton wczytajButton;
    private JTextArea daneFilmuTextArea;
    private JPanel panell;
    private JTextArea tytułTextArea;
    private JTextArea reżyserTextArea1;
    private JTextArea gatunekTextArea1;
    private JTextArea rokTextArea2;

    public WindowPanel() {
        wczytajButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);

            }
        });
    }

    public JPanel getPanel1() {
        return panel1;
    }
}


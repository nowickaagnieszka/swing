package com.sda.swing.Filmy;

import javax.swing.*;
import java.awt.*;

public class Window {
    private JFrame frame;
    private WindowPanel panel;

    public Window() {
        this.panel = new WindowPanel();
        this.frame = new JFrame();
        this.frame.setContentPane(panel.getPanel1());
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setPreferredSize(new Dimension(640,480));
        this.frame.pack();
    }
    public void setVisible(boolean b) {
        frame.setVisible(b);
    }

}

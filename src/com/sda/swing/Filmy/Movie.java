package com.sda.swing.Filmy;

public class Movie {
    private String title;
    private String director;
    private String year;
    private String type;
    private double rating;

    public Movie(String line) {
        this.title = line.split(";;;")[0];
        this.director = line.split(";;;")[1];
        this.year = line.split(";;;")[2];
        this.type = line.split(";;;")[3];
        this.rating = Double.parseDouble(line.split(";;;")[4]);
    }

    public String getTitle() { return title; }

    public String getDirector() { return director; }

    public String getYear() { return year; }

    public String getType() { return type; }

    public double getRating() { return rating; }
}

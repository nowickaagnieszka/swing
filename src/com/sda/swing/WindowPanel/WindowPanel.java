package com.sda.swing.WindowPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class WindowPanel {
    private JButton button;
    private JPanel mainPanel;
    private JLabel label_center;
    private int counter =0;

    public WindowPanel() {
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Jeden klik");
                counter++;
                label_center.setText("Hello World! "+counter);

                JOptionPane.showMessageDialog(null, "Once again?", "Uwaga komunikat!", JOptionPane.WARNING_MESSAGE);

                super.mouseClicked(e);
            }
        });

    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

}

package com.sda.swing.WindowPanel;

import javax.swing.*;
import java.awt.*;

public class Window {
    private JFrame frame;
    private WindowPanel panel;

    public Window() {
        this.panel = new WindowPanel();

        this.frame = new JFrame();
        this.frame.setContentPane(this.panel.getMainPanel());   //JFrame dziedzicza po konterach

        this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); //rozw.problemu zamk okienek

        //   this.frame.setResizable(false);   // gdyby ograniczyc rozciagniecie okna to
        this.frame.setPreferredSize(new Dimension(640, 480));
        this.frame.pack();   //wew.elementy interfejsu rozciagna okno dla swoic potrzeb uzytk
    }

    public void setVisible(boolean b) {
        frame.setVisible(b);
    }
}

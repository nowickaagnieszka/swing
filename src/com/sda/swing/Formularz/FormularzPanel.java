package com.sda.swing.Formularz;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;

public class FormularzPanel {

    private JTextField fieldName;
    private JComboBox stanField;
    private JSpinner spinnerBut;
    private JButton Save;
    private JButton Clear;
    private JTextField fieldAge;
    private JTextField fieldSurname;
    private JRadioButton womanRadio;
    private JRadioButton menRadio;
    private JPanel MainFormularzPanel;

    public FormularzPanel(){

        Save.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Zapisuje");
                try {
                    PrintWriter output = new PrintWriter(new FileWriter("persons.txt", true));
                    output.println("Imie: "+fieldName.getText()+"\n"+
                            "Nazwisko: "+fieldSurname.getText()+"\n"+
                            "Rok urodzenia: "+fieldAge.getText()+"\n"+
                            "Plec: "+(menRadio.isSelected() ? "Mezczyzna":"Kobieta")+"\n"+
                            "Stan: "+stanField.getSelectedItem()+"\n"+
                            "Rozmiar buta: "+spinnerBut.getValue()+"\n"
                    );
                    output.close();
                } catch (IOException p) {
                    p.printStackTrace();
                }
            }
        });

        Clear.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Czyszcze");
                fieldName.setText("");
                fieldSurname.setText("");
                fieldAge.setText("");

                super.mouseClicked(e);
            }
        });
        spinnerBut.setValue(38);

        womanRadio.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stanField.removeAllItems();
                stanField.addItem("Panna");
                stanField.addItem("Zonata");
                stanField.addItem("Wdowa");
            }
        });


        menRadio.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stanField.removeAllItems();
                stanField.addItem("Kawaler");
                stanField.addItem("Zonaty");
                stanField.addItem("Wdowiec");
            }
        });

        spinnerBut.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int currentValue = (int)spinnerBut.getValue();
            }
        });
    }
  public JPanel getMainFormularzPanel() {
        return MainFormularzPanel;
    }


}

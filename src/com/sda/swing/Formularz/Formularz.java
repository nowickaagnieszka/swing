package com.sda.swing.Formularz;

import javax.swing.*;
import java.awt.*;

public class Formularz {
    private JFrame frame;
    private FormularzPanel fpanel;

    public Formularz() {
        this.fpanel = new FormularzPanel();

        this.frame = new JFrame();
        this.frame.setContentPane(this.fpanel.getMainFormularzPanel());

        this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.frame.setPreferredSize(new Dimension(640, 480));
        this.frame.pack();
    }



    public void setVisible(boolean b) {
        frame.setVisible(b);
    }
}

